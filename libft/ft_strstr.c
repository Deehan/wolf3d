/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 16:48:35 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:49:41 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strstr(const char *a, const char *b)
{
	int	i;
	int	j;

	if (a == NULL)
		return (NULL);
	i = 0;
	j = 0;
	while (a[i])
	{
		if (b[j] == a[i] && b[j])
			j++;
		else if (b[j] != a[i] && b[j] && a[i])
		{
			j = 0;
		}
		else if (!b[j])
			return ((char*)a + i - j);
		i++;
	}
	if (!b[j])
		return ((char*)a + i - j);
	else
		return (NULL);
}
