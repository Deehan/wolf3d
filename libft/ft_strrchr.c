/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 17:52:00 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:49:51 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char*ft_strrchr(const char *s, int c)
{
	size_t	i;
	char	*src;

	src = (char*)s;
	i = ft_strlen(src);
	while ((i > 0) && (src[i] != (char)c))
		i--;
	if (src[i] == (char)c)
		return (&src[i]);
	else
		return (NULL);
}
