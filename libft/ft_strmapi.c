/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 11:24:47 by dnguyen           #+#    #+#             */
/*   Updated: 2015/03/30 19:30:04 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	i;
	char			*res;

	res = NULL;
	if (s != NULL && f != NULL)
	{
		i = 0;
		res = ft_strnew(ft_strlen(s));
		if (res != NULL)
		{
			while (s[i])
			{
				res[i] = (*f)(i, s[i]);
				i++;
			}
		}
	}
	return (res);
}
