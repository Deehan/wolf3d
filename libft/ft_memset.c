/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 11:24:31 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:52:09 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memset(void *b, int c, size_t length)
{
	unsigned char	*cb;
	size_t			i;

	if (b == NULL)
		return (NULL);
	cb = (unsigned char *)b;
	i = 0;
	while (i < length && (cb + i) != NULL)
	{
		*(cb + i) = (unsigned char)c;
		i++;
	}
	return (b);
}
