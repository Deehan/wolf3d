/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 18:22:01 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:49:55 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strnstr(const char *a, const char *b, size_t n)
{
	size_t i;
	size_t j;

	if (a == NULL)
		return (NULL);
	if (b == NULL)
		return ((char*)a);
	i = 0;
	j = 0;
	while (a[i] && i < n && j < n)
	{
		if (b[j] == a[i] && b[j])
			j++;
		else if (b[j] != a[i] && b[j] && a[i])
			j = 0;
		else if (!b[j])
			return ((char*)a + i - j);
		i++;
	}
	if (!b[j])
		return ((char*)a + i - j);
	else
		return (NULL);
}
