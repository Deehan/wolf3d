/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 13:01:10 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:49:47 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int		countwords(char const *s, unsigned char c, int inword)
{
	int		i;
	int		count;

	i = 0;
	count = 0;
	while (s[i])
	{
		while (s[i] && (unsigned char)s[i] == c)
		{
			if (inword)
				inword = 0;
			i++;
		}
		while (s[i] && (unsigned char)s[i] != c)
		{
			if (!inword)
			{
				count++;
				inword = 1;
			}
			i++;
		}
	}
	return (count);
}

static char		**buildtab(char const *s, unsigned char c, int null)
{
	char	**ntab;
	int		length;

	if (null)
	{
		length = 1;
		ntab = (char**)malloc(sizeof(char*) * 1);
	}
	else
	{
		length = countwords(s, c, 0) + 1;
		ntab = (char**)malloc(sizeof(char*) * length);
	}
	if (ntab != NULL)
	{
		while (length > 0)
		{
			ntab[length - 1] = NULL;
			length--;
		}
	}
	return (ntab);
}

static int		isempty(char const *s, unsigned char c)
{
	int		i;

	i = 0;
	if (s[i] == '\0' && c == '\0')
		return (0);
	else if (s[i] == '\0')
		return (1);
	while (s[i])
	{
		if (s[i] != c)
			return (0);
		else
			i++;
	}
	return (1);
}

char			**ft_strsplit(char const *s, char c)
{
	char				**tres;
	unsigned int		begin;
	unsigned int		end;
	unsigned int		i;
	unsigned int		ti;

	i = 0;
	ti = 0;
	if (s == NULL)
		return (NULL);
	if (isempty(s, (unsigned char)c))
		return (buildtab("", (unsigned char)c, 1));
	tres = buildtab(s, (unsigned char)c, 0);
	while (s[i])
	{
		while (s[i] && (unsigned char)s[i] == (unsigned char)c)
			i++;
		begin = i;
		while (s[i] && (unsigned char)s[i] != (unsigned char)c)
			i++;
		end = i;
		if (s[begin])
			tres[ti++] = ft_strsub(s, begin, end - begin);
	}
	return (tres);
}
