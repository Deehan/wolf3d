/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 12:22:50 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:48:31 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strtrim(char const *s)
{
	int		begin;
	int		end;

	begin = 0;
	if (s == NULL)
		return (NULL);
	while (s[begin] && (s[begin] == ' ' || s[begin] == '\n' \
				|| s[begin] == '\t'))
		begin++;
	if ((unsigned int)begin == ft_strlen(s))
		return (ft_strnew(1));
	end = ft_strlen((char*)s) - 1;
	while (end > begin \
			&& (s[end] == ' ' || s[end] == '\n' || s[end] == '\t'))
		end--;
	if (end >= begin)
		return (ft_strsub(s, (unsigned int)begin, (size_t)(end - begin) + 1));
	else
		return (ft_strnew(1));
}
