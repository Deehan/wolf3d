/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 13:19:55 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:52:41 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static void		reverse(char *line, int n)
{
	size_t	l;
	size_t	i;
	char	*tmp;

	i = 0;
	if (n < 0)
		line[ft_strlen(line)] = '-';
	tmp = ft_strdup(line);
	l = ft_strlen(tmp) - 1;
	while (l)
	{
		line[i] = tmp[l];
		i++;
		l--;
	}
	line[i] = tmp[l];
	free(tmp);
}

static void		filltab(int n, char *line)
{
	int		i;

	i = 0;
	while (n >= 10 || n <= -10)
	{
		line[i] = (n >= 0) ? (n % 10) + '0' : '0' - (n % 10);
		n /= 10;
		i++;
	}
	line[i] = (n >= 0) ? (n % 10) + '0' : '0' - (n % 10);
}

char			*ft_itoa(int n)
{
	char	*res;

	res = ft_strnew(21);
	filltab(n, res);
	reverse(res, n);
	return (res);
}
