/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 14:08:46 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:51:14 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strcpy(char *dst, char *src)
{
	int i;

	i = 0;
	if (dst == NULL && src == NULL)
		return (NULL);
	if (src == NULL)
		return (dst);
	if (dst == NULL)
		return (dst);
	while (src[i] != '\0')
	{
		dst[i] = src[i];
		i++;
	}
	if (src[i] == '\0')
		dst[i] = '\0';
	return (dst);
}
