/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 18:05:00 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:52:30 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t			i;
	unsigned char	*rs;

	rs = (unsigned char*)s;
	i = 0;
	while (i < n)
	{
		if (rs[i] == (unsigned char)c)
			return (rs + i);
		i++;
	}
	return (NULL);
}
