/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 12:08:05 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:48:39 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strsub(char const *str, unsigned int start, size_t length)
{
	size_t	i;
	char	*res;

	if (str == NULL)
		return (NULL);
	i = 0;
	res = ft_strnew(length + 1);
	while (str[i + start] && i < length)
	{
		res[i] = str[i + start];
		i++;
	}
	return (res);
}
