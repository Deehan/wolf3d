/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 10:01:39 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:52:13 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char	*d;
	unsigned char	*s;
	unsigned int	i;

	d = (unsigned char*)dst;
	s = (unsigned char*)src;
	i = len;
	if (dst <= src)
		return (ft_memcpy(dst, src, len));
	while (i--)
		d[i] = s[i];
	return (dst);
}
