/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 11:01:59 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:50:42 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t		lsrc;
	size_t		ldst;

	lsrc = ft_strlen((char*)src);
	ldst = ft_strlen(dst);
	if (size <= ldst)
		return (lsrc + size);
	if (lsrc < (size - ldst))
	{
		ft_strncpy(dst + ldst, src, lsrc);
		dst[ldst + lsrc] = '\0';
	}
	else
	{
		ft_strncpy(dst + ldst, src, size - ldst - 1);
		dst[size - 1] = '\0';
	}
	return (ldst + lsrc);
}
