/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 17:38:02 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:51:28 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strchr(const char *s, int c)
{
	size_t	i;
	char	*src;

	i = 0;
	src = (char*)s;
	while ((src[i] != (char)c) && (src[i]))
		i++;
	if (src[i] == (char)c)
		return (&src[i]);
	else
		return (NULL);
}
