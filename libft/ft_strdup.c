/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 13:24:40 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:51:05 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strdup(const char *src)
{
	char	*dst;

	if (src == NULL)
		return (NULL);
	dst = (char*)malloc(sizeof(char) * (ft_strlen(src) + 1));
	if (dst == NULL)
		return (dst);
	ft_strcpy(dst, (char*)src);
	dst[ft_strlen(src)] = '\0';
	return (dst);
}
