/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 12:01:28 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:50:04 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_strnequ(char const *s1, char const *s2, size_t size)
{
	if (s1 == NULL && s2 == NULL)
		return (0);
	if (size == 0)
		return (1);
	return ((ft_strncmp(s1, s2, size) == 0) ? 1 : 0);
}
