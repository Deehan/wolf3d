/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 10:33:49 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:53:07 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_atoi(const char *s)
{
	int		polarity;
	int		res;
	int		i;

	if (s == NULL || !*s)
		return (0);
	i = 0;
	res = 0;
	while (s[i] && (s[i] == ' ' || s[i] == '\n' || s[i] == '\t' \
			|| s[i] == '\v' || s[i] == '\r' || s[i] == '\f'))
		i++;
	polarity = (s[i] == '-' ? -1 : 1);
	if (s[i] == '-' || s[i] == '+')
		i++;
	while (s[i] == '0')
		i++;
	while (s[i] && ft_isdigit(s[i]))
	{
		res = res * 10;
		res += (s[i] - '0');
		i++;
	}
	return (res * polarity);
}
