/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 19:20:58 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/23 12:53:40 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		myjoin(int const fd, char **buf, char **line)
{
	size_t	i;
	char	*joined;

	i = 0;
	while (buf[1][i] != '\n' && buf[1][i] != '\0')
		i++;
	joined = ft_strsub(buf[1], 0, i);
	*line = ft_strjoin(*line, joined);
	free(joined);
	if (buf[1][i] == '\n')
	{
		buf[1] += i + 1;
		return (1);
	}
	else if (buf[1][i] == '\0')
	{
		buf[1] += i;
		return (myread(fd, buf, line));
	}
	return (0);
}

int		myread(int const fd, char **buf, char **line)
{
	size_t	length;
	char	*temp;

	if (buf[1] == NULL || buf[1][0] == '\0')
	{
		if ((length = read(fd, buf[0], BUFF_SIZE)) == 0)
			return (0);
		buf[0][length] = '\0';
		buf[1] = buf[0];
	}
	temp = *line;
	if (myjoin(fd, buf, line) == 1)
	{
		free(temp);
		return (1);
	}
	else
		free(temp);
	return (0);
}

int		get_next_line(int const fd, char **line)
{
	static char		*buf[2];

	if (BUFF_SIZE < 1)
		return (-1);
	if (buf[0] == NULL)
		buf[0] = ft_strnew(BUFF_SIZE + 1);
	if (buf[0] != NULL && (*line = ft_strnew(1)) != NULL)
		return (myread(fd, buf, line));
	return (-1);
}
