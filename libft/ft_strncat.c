/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 14:40:53 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/19 16:50:23 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strncat(char *a, char const *b, size_t n)
{
	size_t i;
	size_t l;

	i = 0;
	l = ft_strlen (a);
	while (b[i] != '\0' && i < n)
	{
		a[i + l] = b[i];
		i++;
	}
	a[i + l] = '\0';
	return (a);
}
