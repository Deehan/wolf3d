# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/12/16 16:21:50 by dnguyen           #+#    #+#              #
#    Updated: 2015/03/30 19:47:32 by dnguyen          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
SRC_PATH	=	./src/
SRC_NAME	=	data_manip.c			\
				ft_draw_octant_0_3.c	\
				ft_draw_octant_4_7.c	\
				get_field.c				\
				raycaster.c				\
				error_manage.c			\
				keycodes.c				\
				main.c
OBJ_PATH	=	./obj/
INC_PATH	=	./include/
LIB_PATH	=	./libft/
LIB_NAME	=	libft.a
NAME		=	wolf3d
LIB			=	-L./minilibx_macos -lmlx -L./libft -lft -framework OpenGL -framework AppKit
CC			=	clang
CFLAGS		=	-Werror -Wall -Wextra
OBJ_NAME	=	$(SRC_NAME:.c=.o)
SRC			=	$(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ			=	$(addprefix $(OBJ_PATH),$(OBJ_NAME))
INC			=	$(addprefix -I,$(INC_PATH))
SHELL		=	bash

all:			libcompile prefix $(NAME) suffix

libcompile:
	@make -C $(LIB_PATH)

prefix:
	@echo -n "Wolf3D Compilation [START] /"

suffix:
	@echo "/ [DONE]"

$(NAME):		$(OBJ)
	@$(CC) $(CFLAGS) $(LIB) $(INC) -o $(NAME) $(LIB_PATH)$(LIB_NAME) $(OBJ)

$(OBJ_PATH)%.o:	$(SRC_PATH)%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
	@$(CC) $(CFLAGS) $(INC) -o $@ -c $<
	@echo -n "*"

clean:
	@make clean -C $(LIB_PATH)
	@rm -f $(OBJ) 2> /dev/null || echo "" > /dev/null
	@rm -rf $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
	@echo "Wolf3D Cleaned"

fclean:
	@make fclean -C $(LIB_PATH)
	@rm -f $(OBJ)
	@rm -f $(NAME)
	@rm -rf $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
	@echo "Wolf3D Fucking Cleaned"

re:				fclean all

.PHONY:			clean fclean re all
