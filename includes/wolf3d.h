/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/19 11:15:09 by dnguyen           #+#    #+#             */
/*   Updated: 2015/03/30 19:50:06 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include "libft.h"
# include "../minilibx_macos/mlx.h"
# include <stdio.h>
# include <math.h>
# include <fcntl.h>
# include <stdio.h>


# define XMAX 1200
# define YMAX 800
# define SPEED 0.07
# define TURN_SPEED 0.03

typedef struct s_graph		t_graph;
typedef struct s_segment	t_segment;

struct		s_segment
{
	int		xstart;
	int		ystart;
	int		xend;
	int		yend;
};

struct		s_graph
{
	int		**field;
	int		field_size;
	int		bpp;
	int		sl;
	int		end;
	void	*mlx;
	void	*win;
	void	*img;
	char	*image;
	int		mapx;
	int		mapy;
	int		stepx;
	int		stepy;
	int		hit;
	int		side;
	int		lineheight;
	int		drawstart;
	int		drawend;
	int		color;
	int		forward;
	int		back;
	int		left;
	int		right;
	int		mleft;
	int		mright;
	int		quit;
	double	movespeed;
	double	rotspeed;
	double	posx;
	double	posy;
	double	dirx;
	double	diry;
	double	planex;
	double	planey;
	double	rayposx;
	double	rayposy;
	double	raydirx;
	double	raydiry;
	double	sidedistx;
	double	sidedisty;
	double	deltadistx;
	double	deltadisty;
};

void		read_error(int retour);
void		free_field(t_graph *g);
void		get_field(t_graph *g, char *path);

void		raycasting_loop(t_graph *g);
void		forward(t_graph *g);
void		backward(t_graph *g);
void		turn_right(t_graph *g);
void		turn_left(t_graph *g);

void		drawvertline(int x, t_graph g);
void		pixlay(t_graph g, int x, int y);
void		ft_drawline(t_segment seg, t_graph g);
void		ft_drawline_0(t_segment seg, t_graph g);
void		ft_drawline_1(t_segment seg, t_graph g);
void		ft_drawline_2(t_segment seg, t_graph g);
void		ft_drawline_3(t_segment seg, t_graph g);
void		ft_drawline_4(t_segment seg, t_graph g);
void		ft_drawline_5(t_segment seg, t_graph g);
void		ft_drawline_6(t_segment seg, t_graph g);
void		ft_drawline_7(t_segment seg, t_graph g);

#endif
