/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 09:26:07 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/30 15:06:23 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <stdlib.h>
# include <unistd.h>

# define BUFF_SIZE 1024

void		*ft_memset(void *b, int c, size_t length);
void		ft_bzero(void *s, size_t n);
void		*ft_memcpy(void *dst, const void *src, size_t n);
void		*ft_memccpy(void *dst, const void *src, int c, size_t n);
void		*ft_memmove(void *dst, const void *src, size_t len);
void		*ft_memchr(const void *s, int c, size_t n);
int			ft_memcmp(const void *s1, const void *s2, size_t n);
size_t		ft_strlen(char const *s);
char		*ft_strdup(const char *src);
char		*ft_strcpy(char *dst, char *src);
char		*ft_strncpy(char *dst, const char *src, size_t n);
char		*ft_strcat(char *a, char *b);
char		*ft_strncat(char *a, char const *b, size_t n);
size_t		ft_strlcat(char *a, char const *b, size_t n);
char		*ft_strchr(const char *s, int c);
char		*ft_strrchr(const char *s, int c);
char		*ft_strstr(const char *a, const char *b);
char		*ft_strnstr(const char *a, const char *b, size_t n);
int			ft_strcmp(const char *s1, const char *s2);
int			ft_strncmp(const char *s1, const char *s2, size_t n);
int			ft_atoi(const char *str);
int			ft_isalpha(int c);
int			ft_isdigit(int c);
int			ft_isalnum(int c);
int			ft_isascii(int c);
int			ft_isprint(int c);
int			ft_toupper(int c);
int			ft_tolower(int c);
void		*ft_memalloc(size_t size);
void		ft_memdel(void **ap);
char		*ft_strnew(size_t size);
void		ft_strdel(char **as);
void		ft_strclr(char *s);
void		ft_striter(char *s, void (*f)(char*));
void		ft_striteri(char *s, void (*f)(unsigned int, char *));
char		*ft_strmap(char const *s, char (*f)(char));
char		*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int			ft_strequ(char const *s1, char const *s2);
int			ft_strnequ(char const *s1, char const *s2, size_t n);
char		*ft_strsub(char const *str, unsigned int start, size_t length);
char		*ft_strjoin(char const *s1, char const *s2);
char		*ft_strtrim(char const *s);
char		**ft_strsplit(char const *s, char c);
char		*ft_itoa(int n);
char		**ft_strsplit(char const *s, char c);
void		ft_putendl(char const *s);
void		ft_putchar_fd(char c, int fd);
void		ft_putchar(char c);
void		ft_putstr_fd(char *str, int fd);
void		ft_putstr(char *str);
void		ft_putnbr_fd(int i, int fd);
void		ft_putnbr(int i);
int			myjoin(int const fd, char **buf, char **line);
int			myread(int const fd, char **buf, char **line);
int			get_next_line(int const fd, char **line);

#endif
