/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keycodes.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 14:51:48 by dnguyen           #+#    #+#             */
/*   Updated: 2015/02/12 18:37:57 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void	forward(t_graph *g)
{
	if (!(g->field)[(int)(g->posx + g->dirx * SPEED)][(int)g->posy])
		g->posx += g->dirx * SPEED;
	if (!(g->field)[(int)(g->posx)][(int)(g->posy + g->diry * SPEED)])
		g->posy += g->diry * SPEED;
}

void	backward(t_graph *g)
{
	if (!(g->field)[(int)(g->posx - g->dirx * SPEED)][(int)g->posy])
		g->posx -= g->dirx * SPEED;
	if (!(g->field)[(int)(g->posx)][(int)(g->posy - g->diry * SPEED)])
		g->posy -= g->diry * SPEED;
}

void	turn_right(t_graph *g)
{
	double	dx;
	double	px;

	dx = g->dirx;
	g->dirx = g->dirx * cos(-TURN_SPEED) - g->diry * sin(-TURN_SPEED);
	g->diry = dx * sin(-TURN_SPEED) + g->diry * cos(-TURN_SPEED);
	px = g->planex;
	g->planex = g->planex * cos(-TURN_SPEED) - g->planey *
		sin(-TURN_SPEED);
	g->planey = px * sin(-TURN_SPEED) + g->planey *
		cos(-TURN_SPEED);
}

void	turn_left(t_graph *g)
{
	double	dx;
	double	px;

	dx = g->dirx;
	g->dirx = g->dirx * cos(TURN_SPEED) - g->diry * sin(TURN_SPEED);
	g->diry = dx * sin(TURN_SPEED) + g->diry * cos(TURN_SPEED);
	px = g->planex;
	g->planex = g->planex * cos(TURN_SPEED) - g->planey * sin(TURN_SPEED);
	g->planey = px * sin(TURN_SPEED) + g->planey * cos(TURN_SPEED);
}
