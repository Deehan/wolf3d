/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycaster.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/23 11:17:04 by dnguyen           #+#    #+#             */
/*   Updated: 2015/02/09 12:28:30 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void	calc_ray(t_graph *g, int *x)
{
	double cam;

	cam = 2 * (*x) / (double)XMAX - 1;
	g->rayposx = g->posx;
	g->rayposy = g->posy;
	g->raydirx = g->dirx + g->planex * cam;
	g->raydiry = g->diry + g->planey * cam;
	g->mapx = (int)g->rayposx;
	g->mapy = (int)g->rayposy;
	g->deltadistx = sqrt(1 + (g->raydiry * g->raydiry) /
			(g->raydirx * g->raydirx));
	g->deltadisty = sqrt(1 + (g->raydirx * g->raydirx) /
			(g->raydiry * g->raydiry));
	g->hit = 0;
}

void	calc_step(t_graph *g)
{
	if (g->raydirx < 0)
	{
		g->stepx = -1;
		g->sidedistx = (g->rayposx - g->mapx) * g->deltadistx;
	}
	else
	{
		g->stepx = 1;
		g->sidedistx = (g->mapx + 1.0 - g->rayposx) * g->deltadistx;
	}
	if (g->raydiry < 0)
	{
		g->stepy = -1;
		g->sidedisty = (g->rayposy - g->mapy) * g->deltadisty;
	}
	else
	{
		g->stepy = 1;
		g->sidedisty = (g->mapy + 1.0 - g->rayposy) * g->deltadisty;
	}
}

void	dda(t_graph *g)
{
	while (g->hit == 0)
	{
		if (g->sidedistx < g->sidedisty)
		{
			g->sidedistx += g->deltadistx;
			g->mapx += g->stepx;
			g->side = 0;
		}
		else
		{
			g->sidedisty += g->deltadisty;
			g->mapy += g->stepy;
			g->side = 1;
		}
		if ((g->field)[g->mapx][g->mapy] > 0)
			g->hit = 1;
	}
}

void	calc_dist(t_graph *g)
{
	double	walldist;

	if (g->side == 0)
		walldist = fabs(((double)g->mapx - g->rayposx +
					(1 - (double)g->stepx) / 2) / g->raydirx);
	else
		walldist = fabs(((double)g->mapy - g->rayposy +
					(1 - (double)g->stepy) / 2) / g->raydiry);
	g->lineheight = abs((int)(YMAX / walldist));
	g->drawstart = -(g->lineheight) / 2 + YMAX / 2;
	if (g->drawstart < 0)
		g->drawstart = 0;
	g->drawend = (int)(g->lineheight / 2 + YMAX / 2);
	if (g->drawend >= YMAX)
		g->drawend = YMAX - 1;
	if (g->side == 0 && (int)g->posx >= g->mapx)
		g->color = 0xff0000;
	else if (g->side == 0 && (int)g->posx < g->mapx)
		g->color = 0xff00;
	else if (g->side == 1 && (int)g->posy >= g->mapy)
		g->color = 0xff;
	else if (g->side == 1 && (int)g->posy < g->mapy)
		g->color = 0xffff00;
}

void	raycasting_loop(t_graph *g)
{
	int x;

	x = 0;
	while (++x < XMAX)
	{
		calc_ray(g, &x);
		calc_step(g);
		dda(g);
		calc_dist(g);
		drawvertline(x, *g);
	}
}
