/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_octant_4_7.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/27 19:52:15 by dnguyen           #+#    #+#             */
/*   Updated: 2015/01/30 18:54:20 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void	ft_drawline_4(t_segment seg, t_graph g)
{
	int			dx;
	int			dy;
	double		e;

	e = seg.xend - seg.xstart;
	dx = e * 2;
	dy = (seg.yend - seg.ystart) * 2;
	while (seg.xstart >= seg.xend)
	{
		pixlay(g, seg.xstart, seg.ystart);
		seg.xstart--;
		if ((e -= dy) >= 0)
		{
			seg.ystart--;
			e += dx;
		}
	}
}

void	ft_drawline_5(t_segment seg, t_graph g)
{
	int			dx;
	int			dy;
	double		e;

	e = seg.yend - seg.ystart;
	dy = e * 2;
	dx = (seg.xend - seg.xstart) * 2;
	while (seg.ystart >= seg.yend)
	{
		pixlay(g, seg.xstart, seg.ystart);
		(seg.ystart)--;
		if ((e -= dx) >= 0)
		{
			seg.xstart--;
			e += dy;
		}
	}
}

void	ft_drawline_6(t_segment seg, t_graph g)
{
	int			dx;
	int			dy;
	double		e;

	e = seg.yend - seg.ystart;
	dy = e * 2;
	dx = (seg.xend - seg.xstart) * 2;
	while (seg.ystart >= seg.yend)
	{
		pixlay(g, seg.xstart, seg.ystart);
		seg.ystart--;
		if ((e += dx) > 0)
		{
			seg.xstart++;
			e += dy;
		}
	}
}

void	ft_drawline_7(t_segment seg, t_graph g)
{
	int			dx;
	int			dy;
	double		e;

	e = seg.xend - seg.xstart;
	dx = e * 2;
	dy = (seg.yend - seg.ystart) * 2;
	while (seg.xstart <= seg.xend)
	{
		pixlay(g, seg.xstart, seg.ystart);
		seg.xstart++;
		if ((e += dy) < 0)
		{
			seg.ystart--;
			e += dx;
		}
	}
}
