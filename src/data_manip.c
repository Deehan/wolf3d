/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_manip.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/19 12:57:53 by dnguyen           #+#    #+#             */
/*   Updated: 2015/02/09 15:15:15 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

int		ft_octant_definer(t_segment seg)
{
	int			dx;
	int			dy;

	dx = seg.xend - seg.xstart;
	dy = seg.yend - seg.ystart;
	if (dx >= 0 && dy >= 0 && dx >= dy)
		return (0);
	else if (dx >= 0 && dy > 0 && dx <= dy)
		return (1);
	else if (dx <= 0 && dy >= 0 && -dx < dy)
		return (2);
	else if (dx <= 0 && dy >= 0 && -dx >= dy)
		return (3);
	else if (dx < 0 && dy < 0 && dx <= dy)
		return (4);
	else if (dx <= 0 && dy < 0 && dx > dy)
		return (5);
	else if (dx >= 0 && dy < 0 && dx < -dy)
		return (6);
	else if (dx >= 0 && dy <= 0 && dx >= -dy)
		return (7);
	else
		return (-1);
}

void	ft_drawline(t_segment seg, t_graph g)
{
	int			octant;

	octant = ft_octant_definer(seg);
	if (octant == 0)
		ft_drawline_0(seg, g);
	else if (octant == 1)
		ft_drawline_1(seg, g);
	else if (octant == 2)
		ft_drawline_2(seg, g);
	else if (octant == 3)
		ft_drawline_3(seg, g);
	else if (octant == 4)
		ft_drawline_4(seg, g);
	else if (octant == 5)
		ft_drawline_5(seg, g);
	else if (octant == 6)
		ft_drawline_6(seg, g);
	else if (octant == 7)
		ft_drawline_7(seg, g);
}

int		getpos(int x, int y)
{
	if (x < 1 || x > XMAX - 1 || y < 1 || y > YMAX - 1)
		return (-1);
	return (y * XMAX + x);
}

void	pixlay(t_graph g, int x, int y)
{
	int			index;

	g.image = mlx_get_data_addr(g.img, &(g.bpp), &(g.sl), &(g.end));
	index = getpos(x, y) * (g.bpp / 8);
	if (index >= 0)
	{
		g.image[index + 2] = g.color >> 16 & 0xff;
		g.image[index + 1] = g.color >> 8 & 0xff;
		g.image[index] = g.color & 0xff;
		g.image[index + 3] = 0;
	}
}

void	drawvertline(int x, t_graph g)
{
	t_segment	seg;

	seg.xstart = x;
	seg.ystart = g.drawstart;
	seg.xend = x;
	seg.yend = g.drawend;
	ft_drawline(seg, g);
	seg.ystart = 0;
	seg.yend = g.drawstart;
	g.color = 0xffffff;
	ft_drawline(seg, g);
	seg.ystart = g.drawend;
	seg.yend = YMAX;
	g.color = 0x999999;
	ft_drawline(seg, g);
}
