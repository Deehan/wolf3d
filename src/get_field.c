/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_field.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/23 11:20:37 by dnguyen           #+#    #+#             */
/*   Updated: 2015/02/04 12:03:30 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

int		*parse(char *line, int length)
{
	int i;
	int *strike;

	i = 0;
	strike = NULL;
	if ((strike = (int*)malloc(sizeof(int) * length)) == NULL)
		return (NULL);
	while (i < length)
	{
		strike[i] = line[i] - '0';
		i++;
	}
	return (strike);
}

int		**getmap(int fd, t_graph *g)
{
	int		**field;
	int		i;
	int		ret;
	char	*line;

	i = 0;
	field = NULL;
	ret = get_next_line(fd, &line);
	if (((field = (int**)malloc(sizeof(int*) * ft_strlen(line))) == NULL)
			|| ret <= 0)
		return (NULL);
	g->field_size = (ret > 0) ? ft_strlen(line) : 0;
	while (ret > 0)
	{
		if (line)
		{
			field[i++] = parse(line, ft_strlen(line));
			free(line);
			line = NULL;
		}
		ret = get_next_line(fd, &line);
	}
	if (line)
		free(line);
	return (field);
}

void	get_field(t_graph *g, char *path)
{
	int		fd;

	if ((fd = open(path, O_RDONLY)) == -1)
	{
		perror(path);
		exit(EXIT_FAILURE);
	}
	g->field = getmap(fd, g);
}
