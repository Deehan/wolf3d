/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/21 11:14:38 by dnguyen           #+#    #+#             */
/*   Updated: 2015/03/30 19:56:06 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void	init_graph(t_graph *g, char *path)
{
	g->posx = 2.0;
	g->posy = 2.0;
	g->dirx = 1.0;
	g->diry = 0.0;
	g->planex = 0.0;
	g->planey = 0.66;
	g->forward = 0;
	g->back = 0;
	g->left = 0;
	g->right = 0;
	get_field(g, path);
}

int		expose_hook(t_graph *g)
{
	mlx_put_image_to_window(g->mlx, g->win, g->img, 0, 0);
	return (0);
}

int		keypress(int keycode, t_graph *g)
{
	if (keycode == 53)
	{
		mlx_destroy_image(g->mlx, g->img);
		free_field(g);
		exit(0);
	}
	if (keycode == 126)
		g->forward = 1;
	else if (keycode == 125)
		g->back = 1;
	if (keycode == 123)
		g->right = 1;
	else if (keycode == 124)
		g->left = 1;
	return (0);
}

int		keyrelea(int keycode, t_graph *g)
{
	if (keycode == 126)
		g->forward = 0;
	else if (keycode == 125)
		g->back = 0;
	if (keycode == 123)
		g->right = 0;
	else if (keycode == 124)
		g->left = 0;
	return (0);
}

int		key_loop(t_graph *g)
{
	if (g->forward)
		forward(g);
	if (g->back)
		backward(g);
	if (g->right)
		turn_right(g);
	if (g->left)
		turn_left(g);
	raycasting_loop(g);
	mlx_put_image_to_window(g->mlx, g->win, g->img, 0, 0);
	return (0);
}

int		main(int ac, char **av)
{
	t_graph	g;

	if (ac != 2)
	{
		write(2, "I need one argument\n", 20);
		return (0);
	}
	if ((g.mlx = mlx_init()) == NULL)
	{
		write(1, "mlx_init failed, no segfault\n", 29);
		exit(EXIT_FAILURE);
	}
	g.win = mlx_new_window(g.mlx, XMAX, YMAX, "Labyrinth");
	g.img = mlx_new_image(g.mlx, XMAX, YMAX);
	init_graph(&g, av[1]);
	if (g.field)
		raycasting_loop(&g);
	else
		exit(EXIT_FAILURE);
//	mlx_key_hook(g.win, key_hook, &g);
	mlx_hook(g.win, 2, (1L<<0), &keypress, &g);
	mlx_hook(g.win, 3, (1L<<1), &keyrelea, &g);
	mlx_loop_hook(g.mlx, &key_loop, &g);
	mlx_expose_hook(g.win, expose_hook, &g);
	mlx_loop(g.mlx);
	return (0);
}
