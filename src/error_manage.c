/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_manage.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dnguyen <dnguyen@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/23 11:28:03 by dnguyen           #+#    #+#             */
/*   Updated: 2015/02/04 12:02:23 by dnguyen          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void	free_field(t_graph *g)
{
	int i;

	i = 0;
	if (g->field)
		while (i < g->field_size)
		{
			free((g->field)[i]);
			i++;
		}
	free(g->field);
}
